# AZWI KEDA role to monitor ASB queue
resource "azurerm_role_assignment" "keda_asb_read" {
  scope                            = module.asb.servicebus_namespace_id
  role_definition_name             = "Azure Service Bus Data Receiver"
  principal_id                     = var.aksAzwiSPNObjId
  # principal_id                     = azuread_service_principal.directory_role_app.object_id
}
