variable "baseTags" {
  default = {
    Owner   = "samko.yoon@servian.com",
    Purpose = "training",
    Client  = "Coles"
  }
  description = "Servian base resource tags"
  type        = map(string)
}

variable "userName" {
  type        = string
  default     = "yourName"
  description = "username used for resource prefix"
}

variable "env" {
  type        = string
  default     = "dev"
  description = "Environment name"
}

variable "aksNamespace" {
  type        = string
  default     = "default"
  description = "AKS namespace to deploy service account"
}

variable "aksServiceAccountName" {
  type        = string
  default     = "workload-identity-sa"
  description = "AKS namespace to deploy service account"
}

variable "aksAdminGroupObjId" {
  type        = string
  description = "AKS admin group object ID"
}

variable "aksAzwiObjId" {
  type        = string
  description = "Azure Workload Identity app object ID"
}

variable "aksAzwiAppId" {
  type        = string
  description = "Azure Workload Identity app application ID"
}

variable "aksAzwiSPNObjId" {
  type        = string
  description = "Azure Workload Identity SPN object ID"
}
