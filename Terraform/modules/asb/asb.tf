resource "azurerm_servicebus_namespace" "asb" {
  name                = "${var.userName}testasb"
  location            = var.location
  resource_group_name = var.rgName
  sku                 = "Basic"

  tags = merge(
    var.baseTags,
    {
      Environment = var.env
  })
}

resource "azurerm_servicebus_queue" "triggerqueue" {
  name         = "orders"
  namespace_id = azurerm_servicebus_namespace.asb.id

  enable_partitioning = true
}