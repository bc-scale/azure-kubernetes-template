# ================ Enable preview features ================ #
resource "null_resource" "enable_oidci_issuer" {
  provisioner "local-exec" {
    command = <<-EOT
      az feature register --name EnableOIDCIssuerPreview --namespace Microsoft.ContainerService
    EOT
  }
}

resource "null_resource" "enable_keda" {
  provisioner "local-exec" {
    command = <<-EOT
      az feature register --name AKS-KedaPreview --namespace Microsoft.ContainerService
    EOT
  }
}

# ================ AKS cluster ================ #
resource "azurerm_kubernetes_cluster" "aksCluster" {
  name                = "${var.userName}TrainingAKSCluster"
  location            = var.location
  resource_group_name = var.rgName
  dns_prefix          = "${var.userName}trainingaks"

  # OIDC issuer required for AZ WI  
  oidc_issuer_enabled = true

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
    # If nodes need private network, assign it to "vnet_subnet_id"
  }

  identity {
    type = "SystemAssigned"
  }

  azure_active_directory_role_based_access_control {
    managed                = true
    azure_rbac_enabled     = true
    admin_group_object_ids = [var.aksAdminGroupObjId]
  }

  tags = merge(
    var.baseTags,
    {
      Environment = var.env
  })

  lifecycle {
    ignore_changes = [tags]
  }

  depends_on = [
    null_resource.enable_oidci_issuer,
    null_resource.enable_keda
  ]
}
