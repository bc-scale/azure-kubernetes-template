output "kubernetes_cluster_name" {
  value = azurerm_kubernetes_cluster.aksCluster.name
}

output "oidc_issuer_url" {
  value = azurerm_kubernetes_cluster.aksCluster.oidc_issuer_url
}

output "kube_config_raw" {
    value = azurerm_kubernetes_cluster.aksCluster.kube_config_raw
    sensitive = true
}

output "host" {
  value = azurerm_kubernetes_cluster.aksCluster.kube_admin_config.0.host
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.aksCluster.kube_admin_config.0.client_certificate
}

output "client_key" {
  value = azurerm_kubernetes_cluster.aksCluster.kube_admin_config.0.client_key
}

output "cluster_ca_certificate" {
  value = azurerm_kubernetes_cluster.aksCluster.kube_admin_config.0.cluster_ca_certificate
}