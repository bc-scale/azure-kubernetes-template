variable "baseTags" {
  description = "Servian base resource tags"
  type        = map(string)
}

variable "userName" {
  type        = string
  description = "username used for resource prefix"
}

variable "env" {
  type        = string
  description = "Environment name"
}

variable "location" {
  type        = string
  description = "Location"
}

variable "rgName" {
  type        = string
  description = "RG name"
}

variable "aksAdminGroupObjId" {
  type        = string
  description = "AKS admin group object ID"
}
