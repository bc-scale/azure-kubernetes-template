resource "helm_release" "azure-workload-identity" {
  name             = "azure-workload-identity"
  repository       = "https://azure.github.io/azure-workload-identity/charts"
  chart            = "workload-identity-webhook"
  namespace        = "azure-workload-identity-system"
  create_namespace = true

  set {
    name  = "azureTenantID"
    value = data.azuread_client_config.current.tenant_id
  }

  depends_on = [
    module.aks
  ]
}

resource "helm_release" "argocd" {
  name             = "argocd"
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  namespace        = "argocd"
  create_namespace = true

  set {
    name  = "server.service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "server.extraArgs"
    value = "{--insecure,--request-timeout=\"5m\"}"
  }

  depends_on = [
    module.aks
  ]
}

resource "helm_release" "keda" {
  name             = "kedacore"
  repository       = "https://kedacore.github.io/charts"
  chart            = "keda"
  namespace        = "keda"
  create_namespace = true

  set {
    name  = "podIdentity.azureWorkload.enabled"
    value = "true"
  }
  set {
    name  = "podIdentity.azureWorkload.clientId"
    value = var.aksAzwiAppId
    # value = azuread_service_principal.directory_role_app.application_id
  }
  set {
    name  = "podIdentity.azureWorkload.tenantId"
    value = data.azuread_client_config.current.tenant_id
  }

  depends_on = [
    module.aks
  ]
}
