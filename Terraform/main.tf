terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.1.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "2.20.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
  }
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "samkoteststorageaccount"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}

# ================ Resource group ================ #
resource "azurerm_resource_group" "baseRG" {
  name     = "${var.userName}Training"
  location = "Australia East"
  tags     = var.baseTags
  lifecycle {
    ignore_changes = [tags]
  }
}

# ================ Modules ================ #
module "aks" {
  source             = "./modules/aks"
  baseTags           = var.baseTags
  userName           = var.userName
  env                = var.env
  aksAdminGroupObjId = var.aksAdminGroupObjId
  location           = azurerm_resource_group.baseRG.location
  rgName             = azurerm_resource_group.baseRG.name
}

module "asb" {
  source             = "./modules/asb"
  baseTags           = var.baseTags
  userName           = var.userName
  env                = var.env
  location           = azurerm_resource_group.baseRG.location
  rgName             = azurerm_resource_group.baseRG.name
}

module "cosmos" {
  source             = "./modules/cosmos"
  baseTags           = var.baseTags
  userName           = var.userName
  env                = var.env
  location           = azurerm_resource_group.baseRG.location
  rgName             = azurerm_resource_group.baseRG.name
  aksAzwiSPNObjId    = var.aksAzwiSPNObjId
}
