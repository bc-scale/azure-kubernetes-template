userName              = "samko"
env                   = "dev"
aksNamespace          = "my-app-dev"
aksServiceAccountName = "workload-identity-sa"

# Required if Devops agent SPN cannot read/write SPN and AD group
aksAdminGroupObjId = "493f741c-2ada-4950-b429-698477a154cf"
aksAzwiAppId       = "7647ce59-7f8c-413b-857e-eea8572ddd88"
aksAzwiObjId       = "a1ecbb1b-b160-4101-96cd-3501e982d117"
aksAzwiSPNObjId    = "92cb6bc8-8bf3-4972-a4ce-1d96d7cbb729"